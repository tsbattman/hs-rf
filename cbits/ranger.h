#ifndef _RANGER_H_
#define _RANGER_H_

#ifdef __cplusplus
extern "C" {
class Forest;
typedef class Forest* forest_t;
#else
typedef void* forest_t;
#endif

enum tree_type {
    tree_probability
  , tree_classification
  , tree_regression
  , tree_survival
};


#ifdef __cplusplus
}
#endif

#endif // _RANGER_H_
