
module Statistics.Models.Rf.Types (
    TreeType(..)
  ) where

data TreeType = TreeProbability | TreeClass | TreeRegression | TreeSurvival
  deriving (Eq, Show, Read)
