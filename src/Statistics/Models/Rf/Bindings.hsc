
module Statistics.Models.Rf.Bindings (
    CTreeType
  , cTreeProbability
  , cTreeClassification
  , cTreeRegression
  , cTreeSurvival
  ) where

import Data.Word (Word32)

#include <ranger.h>

newtype CTreeType = CTreeType #{type enum tree_type}
  deriving (Eq, Show, Read)

#{enum CTreeType, CTreeType
  , cTreeProbability = tree_probability
  , cTreeClassification = tree_classification
  , cTreeRegression = tree_regression
  , cTreeSurvival = tree_survival
  }
